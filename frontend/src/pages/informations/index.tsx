import React from "react";
import NavBar from "../../components/navBar";
import cover from "../../assets/Group7.svg";
import logo from "../../assets/logo.png";
import "./informations.css";

const Informations = () => {
  return (
    <div>
      <NavBar />
      <div className="informationsContainer">
        <div className="coverDiv">
          <img src={cover} className="cover" alt="capa" />
          <h1 className="coverText">
            Entendendo TE, TUSD e o mercado livre de energia
          </h1>
        </div>
        <div className="textContainer">
          <p className="sectionText">
            O que é TE e TUSD? Você já dever ter visto essas duas siglas na sua
            conta de luz mas o que elas querem dizer?
          </p>

          <p className="sectionText">
            Essas são duas tarifas que são usadas para calcular o valor total da
            sua conta de energia. A <strong>TE</strong> é a{" "}
            <strong>tarifa de energia</strong> e está relacionada ao produto que
            foi consumido, nesse caso a energia. Já a <strong>TUSD</strong> é a
            <strong>Tarifa do Uso do Sistema de Distribuição</strong>, ou seja,
            é a taxa destinada para o serviço que a distribuidora da sua cidade
            presta levando energia a sua empresa, através dos postes.
          </p>

          <p className="sectionText">
            Então, se você consumir 300 KWh no mês, você vai encontrar a sua
            conta assim:
            <ul>
              <li>
                <strong>TE</strong> – 300 KWh multiplicados pela tarifa
              </li>
              <li>
                <strong>TUSD</strong> – 300 KWh multiplicados pela tarifa
              </li>
            </ul>
          </p>

          <h2 className="sectionTitle">O mercado livre de energia</h2>

          <p className="sectionText">
            Também conhecido como Ambiente de Contratação Livre (ACL), é o
            ambiente de contratação do setor elétrico brasileiro no qual a
            escolha do fornecedor de energia é livre para o consumidor, podendo
            assim escolher de quem quer comprar a sua energia.
          </p>

          <p className="sectionText">
            Quais as vantagens da adesão ao{" "}
            <strong>mercado livre de energia?</strong>
          </p>

          <p className="sectionSubTitle">
            <strong>Economia</strong>
          </p>
          <p className="sectionText">
            A principal vantagem em aderir ao mercado livre é a redução de
            custos com energia elétrica. A taxa do TUDS deixa de ser paga além
            de receber descontos de no mínimo 0,46% na TE.
          </p>

          <p className="sectionSubTitle">
            <strong>Poder de escolha</strong>
          </p>
          <p className="sectionText">
            No mercado livre a escolha do fornecedor de energia é sua.
          </p>

          <p className="sectionSubTitle">
            <strong>Previsibilidade orçamentária</strong>
          </p>
          <p className="sectionText">
            Com os preços da sua energia estimados durante todo o prazo do
            contrato, a previsibilidade é garantida e você fica menos exposto
            aos aumentos da tarifa.
          </p>

          <p className="sectionSubTitle">
            <strong>Competitividade</strong>
          </p>
          <p className="sectionText">
            Com a redução de gastos com eletricidade da sua empresa, ela se
            torna mais competitiva no mercado em seu ramo de atuação.
          </p>

          <p className="sectionSubTitle">
            <strong>Sustentabilidade</strong>
          </p>
          <p className="sectionText">
            Te ajudamos a promover a sustentabilidade na sua empresa com a
            transição para o mercado livre que atua com fontes renováveis.
          </p>

          <h2 className="sectionTitle">O mercado livre de energia</h2>
          <p className="sectionText">
            Mas apesar dos benefícios, como decidir se o mercado livre é a
            melhor opção para a sua indústria? A fim de sanar essa dúvida
            criamos a calculadora tributária que te mostra o quanto você pagaria
            se estivesse comprando sua energia no mercado livre.
          </p>
        </div>
      </div>
      <div className="logoDiv">
        <img src={logo} alt="logo" className="logo" />
      </div>
    </div>
  );
};

export default Informations;
