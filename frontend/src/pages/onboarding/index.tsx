import React from "react";
import "./onboarding.css";
import ButtonComponent from "../../components/button/index";
import sideImage from "../../assets/panel.png";
import NavBar from "../../components/navBar";

const onBoarding = () => {
  return (
    <div className="container">
      <NavBar />
      <div className="insideContainer">
        <div className="leftSide">
          <div className="leftSide_callToAction">
            <h1 className="leftSide_callToAction--title">
              Energia limpa para indústrias
            </h1>
            <p className="leftSide_callToAction--text">
              A calculadora que facilita a vida da sua indústria. Com a DayLight
              é possível calcular TUSD (Tarifa de Utilização de Serviços de
              Distribuição) e TUST (Tarifa de Utilização de Serviços de
              Transmissão) de uma maneira fácil e descomplicada.
            </p>
            <ButtonComponent
              linkString={"/calculator"}
              text={"Calculadora >"}
              isBlue={true}
            />
          </div>
        </div>
        <div className="rightSide">
          <img src={sideImage} alt="panel" className="sideImage" />
        </div>
      </div>
    </div>
  );
};

export default onBoarding;
