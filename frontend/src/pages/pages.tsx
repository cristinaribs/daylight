/* eslint-disable import/no-anonymous-default-export */
import onboarding from "./onboarding";
import calculator from "./calculator";
import informations from "./informations";
import advancedCalculator from './advancedCalculator';
import result from './result';

export default {
    onboarding,
    calculator,
    informations,
    advancedCalculator,
    result,
}