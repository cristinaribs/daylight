import React from "react";
import "./result.css";
import ButtonComponent from "../../components/button/index";
import NavBar from "../../components/navBar";

const onBoarding = () => {
  return (
    <div className="resultContainer">
      <NavBar />
      <div className="centerResults">
        <div className="resultDiv">
          <div className="card">
            <div className="card_leftSide">
              <div className="card_leftSide--info">
                <div className="card_leftSide--title">Atual</div>
                <div className="card_leftSide--text">
                  o valor da sua conta atualmente é
                </div>
              </div>
            </div>
            <div className="card_rightSide">
              <div className="card_rightSide--text">
                <p>R$200.58</p>
              </div>
            </div>
          </div>
          <div className="card">
            <div className="card_leftSide">
              <div className="card_leftSide--info">
                <div className="card_leftSide--title">Mercado Livre</div>
                <div className="card_leftSide--text">
                  o valor da sua conta no mercado livre
                </div>
              </div>
            </div>
            <div className="card_rightSide">
              <div className="card_rightSide--text">
                <p>R$200.58</p>
              </div>
            </div>
          </div>
        </div>
        <div className="conclusionDiv">
          <h2 className="conclusionDiv_text">
            Esse seria o total economizado se você estivesse hoje no livre
            mercado:
          </h2>

          <h2 className="conslusionDiv_value">R$200,58</h2>

          <ButtonComponent
            linkString="/calculator"
            text="Realizar outro cálculo >"
            isBlue={false}
          />
        </div>
      </div>
    </div>
  );
};

export default onBoarding;
