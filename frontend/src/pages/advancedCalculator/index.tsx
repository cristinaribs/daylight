import React, { useState } from "react";
import ButtonComponent from "../../components/button";
import NavBar from "../../components/navBar";
import {Link} from 'react-router-dom';
import "./advancedCalculator.css";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";

const calculator = () => {
  const currencies = [
    {
      value: 1,
      label: "Light",
    },
    {
      value: 2,
      label: "ENEL RJ",
    },
    {
      value: 3,
      label: "Energisa NF",
    },
  ];

  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [currency, setCurrency] = useState("1");

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCurrency(event.target.value);
  };

  return (
    <body>
      <NavBar />
      <section className="pageContainer">
        <div className="center">
          <div className="titleContainer">
            <h1 className="title">Calculadora Tributária</h1>
          </div>
          <div className="subtitleContainer">
            <p className="subtitle">
              Adicione nos campos abaixo os dados do gasto mensal de energia da
              sua empresa
            </p>
          </div>
        </div>

        <div className="center" id="calculatorSection">
          <Box
            component="form"
            sx={{
              "& .MuiTextField-root": { m: 1, width: "25ch" },
            }}
            noValidate
            autoComplete="off"
          >
            <div className="buttonDivCalc">
              <TextField
                id="Distribuidora"
                select
                label="Select"
                value={currency}
                onChange={handleChange}
              >
                {currencies.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>

              <TextField
                id="Consumo-ponta-Kw/h"
                label="Consumo ponta Kw/h"
                variant="outlined"
              />

              <TextField
                id="Consumo-fora-ponta-Kw/h"
                label="Consumo fora ponta Kw/h"
                variant="outlined"
              />

              <TextField
                id="Demanda-ponta-Kw/h"
                label="Demanda ponta Kw/h"
                variant="outlined"
              />

              <TextField
                id="Demanda-fora-ponta-Kw/h"
                label="Demanda fora ponta Kw/h"
                variant="outlined"
              />
            </div>
          </Box>
        </div>
        <Link to="/result" className="buttonAdvancedCalculator">
          <p className="button_text">Calcular {'>'}</p>
        </Link>
        <Link to="/calculator" className="buttonOrangeAdvancedCalculator">
          <p className="button_text">Calcular apenas com  valor da conta de luz {'>'}</p>
        </Link>
      </section>
    </body>
  );
};

export default calculator;
