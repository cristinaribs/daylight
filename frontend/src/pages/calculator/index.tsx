import React from "react";
import ButtonComponent from "../../components/button";
import NavBar from "../../components/navBar";
import "./calculator.css";
import banner from "../../assets/Group13.png";

const calculator = () => {
  return (
    <body>
      <NavBar />
      <section className="pageContainer">
        <div className="center">
          <div className="titleContainer">
            <h1 className="title">Calculadora Tributária</h1>
          </div>
          <div className="subtitleContainer">
            <p className="subtitle">
              Saiba quanto é possível economizar sem mudar a maneira com que
              você usa sua energia
            </p>
          </div>
        </div>

        <div className="buttonDivCalc">
          <div className="inputVal">
            <ButtonComponent
              linkString="#calculatorSection"
              text="Calcular com conta de luz >"
              isBlue={false}
            />
            <ButtonComponent
              linkString="/acalculator"
              text="Calcular com dados técnicos >"
              isBlue={false}
            />
          </div>
        </div>

        <img src={banner} alt="banner" className="bannerSize" />

        <div className="center" id="calculatorSection">
          <div className="subtitleContainer">
            <p className="subtitle">
              Adicione nos campos abaixo os dados do gasto mensal de energia da
              sua empresa
            </p>
          </div>
          <div className="buttonDivCalc">
            <div className="inputVal">
              <input placeholder="Kw/h" className="input" type="number"></input>{" "}
              <input placeholder="Distribuidora" className="input"></input>
              <ButtonComponent linkString="/result" text="Calcular >" isBlue={true} />
            </div>
          </div>
        </div>
      </section>
    </body>
  );
};

export default calculator;
