import React from "react";
import "./style.css";

interface props {
  linkString: string;
  text: string;
  isBlue: boolean;
}

const ButtonComponent = ({ linkString, text, isBlue }: props) => {
  return (
    <a href={linkString} className={isBlue ? "button" : "button-orange"}>
      <p className="button_text">{text}</p>
    </a>
  ); 
};

export default ButtonComponent;
