import React from "react";
import { Link } from "react-router-dom";
import "./style.css";

const NavBar = () => {
  return (
    <div className="NavBar">
      <Link to={'/'}>
      <img
        className="image"
        src="https://media.discordapp.net/attachments/904011919365668877/982725897402613810/Daylight_logo.png"
        alt="side"
      />
      </Link>

        <ul>
          <li>
            <Link to={"/calculator"}>Calculadora</Link>
          </li>
          <li>
            <Link to={"/info"}>Info</Link>
          </li>
        </ul>
      </div>
  );
};

export default NavBar;