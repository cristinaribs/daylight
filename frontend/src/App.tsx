import React from "react";
import "./App.css";
import pages from "./pages/pages";
import { Route, Routes } from "react-router-dom";

function App() {
  return (
    <Routes>
      <Route path='/' element={<pages.onboarding />}/>
      <Route path='/calculator' element={<pages.calculator/>}/>
      <Route path='/info' element={<pages.informations/>}/>
      <Route path='/acalculator' element={<pages.advancedCalculator/>}/>
      <Route path='/result' element={<pages.result/>}/>
    </Routes>
  );
}

export default App;
