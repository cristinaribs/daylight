# Daylight
Uma calculadora que facilita a vida da sua Industria. 
Com a Daylight é possível calcular o TUSD (Tarifa de Utlização de Serviços de Distribuição) e TUST (Tarifa 
de Serviços de Trasminssão) de uma maneira fácil 
e descomplicada
 
**Status do Projeto** : Terminado

![Badge](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)
![Badge](https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB)
![Badge](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)
 
 
## Tabela de Conteúdo

 1. [Tecnologias utilizadas](#tecnologias-utilizadas)
 2. [Instalação](#instalação)
 3. [Configuração](#configuração)
 4. [Uso](#uso)
 5. [Arquitetura](#arquitetura)
 6. [Autores](#autores)
 
## Tecnologias utilizadas

Essas são as frameworks e ferramentas que você precisará instalar para desenvolver esse projeto:

 - React versão ^18.1.0
 - NodeJs versão ^14.17.6
 - Express versão ^4.18.1
 - Sequelize versão ^6.20.1

## Instalação 

- Clone o repositório;
- Em um terminal, vá para a pasta 'frontend' e execute o seguinte comando:

``` bash
$ yarn install
```
- Agora, vá para a pasta 'backend' e execute o seguinte comando:

``` bash
$ npm install
```

## Configuração

- copie o arquivo '.env.example' para o '.env' executando o seguinte comando:

``` bash
$ cp .env.example .env
```
- Execute o comando a seguir na pasta 'backend':

``` bash
$ npm run migrate
```
 
## Uso

Para interagir com a aplicação será preciso servir o Front-End.

Para servir localmente o Front-End localmente, execute o seguinte comando no diretório Daylight/frontend:

``` bash
$ yarn start
```
## Arquitetura

- [Link para os slides do pitch](https://www.canva.com/design/DAFCq3ssMds/e3KLr_tNA_rSOc7YGUY0CA/view?utm_content=DAFCq3ssMds&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink)
- [Link da prototipagem no figma](https://www.figma.com/file/6x6e6hUa4Al7ZyISbmhA3P/Prot%C3%B3tipo?node-id=0%3A1)

## Autores

* Dev Front-end/ Designer - Ana Carolina Ataliba
* Dev Back-end/ Negócios - Cristina Souza
* Dev Front-end - João Pedro Duarte 
* Dev Front-end/ Designer - Laura Miranda
 

## Última atualização: 05/06/2022